<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Services\PostService;

class PostController extends Controller
{
    protected $postService;
    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    // Show data
    public function index()
    {
        try {
            $result['data'] = $this->postService->getAll();
        } catch (Exception $e) {
            $result = [
                'status' => 500,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    // Create data.
    public function create(Request $request)
    {
        $data = $request->only(['title', 'description']);

        $result = ['status' => 200];

        try {
            $result['data'] = $this->postService->savePostData($data);
        } catch (Exception $e) {
            $result = [
                'status' => 505,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result, $result['status']);
    }

    // Show data by id
    public function show($id)
    {
        try {
            $result['data'] = $this->postService->getById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 505,
                'error' => $e->getMessage()
            ];
        }

        return response()->json($result);
    }

    public function update(Request $request, Post $post)
    {
        //
    }

    public function destroy($id)
    {
        try {
            $result['data'] = $this->postService->deleteById($id);
        } catch (Exception $e) {
            $result = [
                'status' => 505,
                'error' => $e->getMessage()
            ];
        }

        return "Delete Data Success!";
    }
}
