<?php

namespace App\Repositories;

use App\Models\Post;

class PostRepository
{

    protected $post;
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function save(array $data)
    {
        //        $post = new $this->post->create($data);
        $post = new $this->post;

        $post->title = $data['title'];
        $post->description = $data['description'];

        $post->save();

        return $post->fresh();
    }

    // show data
    public function getAllPost()
    {
        return $this->post->get();
    }

    // get data by id
    public function getById(int $id)
    {
        return $this->post
            ->where('id', $id)
            ->get();
    }

    // Delete data post
    public function delete($id)
    {
        $post = $this->post->find($id);
        $post->delete();

        return $post;
    }
}
