<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Repositories\PostRepository;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CrudTest extends TestCase
{

    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_crud()
    {
        // Create Data Posts
        $posts = Post::create([
            "title" => "Barista Carijamu",
            "description" => "Kegiatan ini diadakan secara luring"
        ]);

        // Check data, Is the data already in the database?
        $this->assertDatabaseHas('posts', [
            'title' => 'Barista Carijamu',
            'description' => "Kegiatan ini diadakan secara luring"
        ]);
    }
}
