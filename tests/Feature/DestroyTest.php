<?php

namespace Tests\Feature;

use App\Models\Post;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class DestroyTest
{
    use DatabaseTransactions;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function destroy_test()
    {
        // Delete Data
        $posts = Post::get();
        Post::destroy($posts->id);
    }
}
